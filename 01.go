package main

import (
	"log"
	"strings"
	"unicode"
)

func D01(input string) {
	p1Total := 0
	p2Total := 0

	reverse := func(s string) string {
		n := len(s)
		rev := make([]rune, n)
		for i, r := range s {
			rev[n-i-1] = r
		}
		return string(rev)
	}

	replacements := []string{"one", "1", "two", "2", "three", "3", "four", "4", "five", "5", "six", "6", "seven", "7", "eight", "8", "nine", "9"}
	replacer := strings.NewReplacer(replacements...)

	reverseReplacements := make([]string, len(replacements))
	for i, replacement := range replacements {
		if i%2 == 1 {
			reverseReplacements[i] = replacement
		} else {
			reverseReplacements[i] = reverse(replacement)
		}
	}
	reverseReplacer := strings.NewReplacer(reverseReplacements...)

	p1Parser := func(s string, counter *int) {
		num := 0
		for i := 0; i < len(s); i++ {
			if unicode.IsDigit(rune(s[i])) {
				num += (int(s[i]) - '0') * 10
				break
			}
		}
		for i := len(s) - 1; i >= 0; i-- {
			if unicode.IsDigit(rune(s[i])) {
				num += int(s[i]) - '0'
				break
			}
		}
		*counter += num
	}
	p2Parser := func(s string, counter *int) {
		num := 0
		forwardS := replacer.Replace(s)
		for i := 0; i < len(forwardS); i++ {
			if unicode.IsDigit(rune(forwardS[i])) {
				num += (int(forwardS[i]) - '0') * 10
				break
			}
		}
		reverseS := reverse(reverseReplacer.Replace(reverse(s)))
		for i := len(reverseS) - 1; i >= 0; i-- {
			if unicode.IsDigit(rune(reverseS[i])) {
				num += int(reverseS[i]) - '0'
				break
			}
		}
		*counter += num
	}

	for _, s := range strings.Split(input, "\n") {
		p1Parser(s, &p1Total)
		p2Parser(s, &p2Total)
	}

	log.Printf("P1: %d", p1Total)
	log.Printf("P2: %d", p2Total)
}
