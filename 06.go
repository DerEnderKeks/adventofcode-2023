package main

import (
	"log"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func D06(input string) {
	lines := strings.Split(input, "\n")
	var times []int
	var distances []int
	p2Time := 0
	p2Distance := 0

	splitRegex := regexp.MustCompile("\\s+")
	slices := []*[]int{&times, &distances}
	p2Values := []*int{&p2Time, &p2Distance}
	for i, line := range lines {
		_, valuesS, _ := strings.Cut(line, ":")
		valuesS = strings.Trim(valuesS, " ")
		for _, s := range splitRegex.Split(valuesS, -1) {
			atoi, _ := strconv.Atoi(s)
			*slices[i] = append(*slices[i], atoi)
		}
		p2ValueS := strings.ReplaceAll(valuesS, " ", "")
		atoi, _ := strconv.Atoi(p2ValueS)
		*p2Values[i] = atoi
	}

	calculateRange := func(t, d float64) (int, int) {
		disc := t*t - 4*d
		sqrtD := math.Sqrt(disc)
		t1 := (-t + sqrtD) / -2
		t2 := (-t - sqrtD) / -2

		ti1 := math.Ceil(t1)
		ti2 := math.Floor(t2)

		if t1 == ti1 {
			ti1++
		}
		if t2 == ti2 {
			ti2--
		}

		return int(ti1), int(ti2)
	}

	p1Sum := 1
	var ranges [][]int
	for i := 0; i < len(times); i++ {
		t1, t2 := calculateRange(float64(times[i]), float64(distances[i]))
		ranges = append(ranges, []int{t1, t2})
		p1Sum *= t2 - t1 + 1
	}

	p2T1, p2T2 := calculateRange(float64(p2Time), float64(p2Distance))
	p2Sum := p2T2 - p2T1 + 1

	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
