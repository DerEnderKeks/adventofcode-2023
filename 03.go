package main

import (
	"log"
	"math"
	"regexp"
	"strconv"
	"strings"
)

func D03(input string) {
	lines := strings.Split(input, "\n")

	symbolRegex := regexp.MustCompile("[^.0-9]")
	gearRegex := regexp.MustCompile("\\*")
	numberRegex := regexp.MustCompile("\\d+")

	type Match struct {
		Y    int
		X    int
		Text string
	}

	findMatches := func(symbolRegex *regexp.Regexp, line string, y int) []Match {
		rMatches := symbolRegex.FindAllStringIndex(line, -1)
		var matches []Match
		for _, match := range rMatches {
			matches = append(matches, Match{
				Y:    y,
				X:    match[0],
				Text: line[match[0]:match[1]],
			})
		}
		return matches
	}

	var symbols []Match
	var gears []Match
	var numbers []Match

	for y, line := range lines {
		symbols = append(symbols, findMatches(symbolRegex, line, y)...)
		gears = append(gears, findMatches(gearRegex, line, y)...)
		numbers = append(numbers, findMatches(numberRegex, line, y)...)
	}

	isAdjacent := func(m1, m2 Match) bool {
		return math.Abs(float64(m1.Y-m2.Y)) <= 1 &&
			m1.X <= m2.X+len(m2.Text) &&
			m2.X <= m1.X+len(m1.Text)
	}

	filterSlice := func(s []Match, cond func(Match) bool) []Match {
		var filtered []Match
		for _, match := range s {
			if cond(match) {
				filtered = append(filtered, match)
			}
		}
		return filtered
	}

	p1Sum := 0
	p1Numbers := filterSlice(numbers, func(match Match) bool {
		for _, symbol := range symbols {
			if isAdjacent(match, symbol) {
				return true
			}
		}
		return false
	})
	for _, match := range p1Numbers {
		i, _ := strconv.Atoi(match.Text)
		p1Sum += i
	}

	p2Sum := 0
	for _, gear := range gears {
		adjacentNumbers := filterSlice(numbers, func(match Match) bool {
			return isAdjacent(match, gear)
		})
		if len(adjacentNumbers) == 2 {
			n1, _ := strconv.Atoi(adjacentNumbers[0].Text)
			n2, _ := strconv.Atoi(adjacentNumbers[1].Text)
			p2Sum += n1 * n2
		}
	}

	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
