package main

import (
	"log"
	"strings"
)

func D08(input string) {
	lines := strings.Split(input, "\n")
	instructionsS := lines[0]
	nodesS := lines[2:]

	var instructions []int
	for _, s := range instructionsS {
		if s == 'R' {
			instructions = append(instructions, 1)
		} else {
			instructions = append(instructions, 0)
		}
	}

	type Node struct {
		Left  string
		Right string
	}
	nodes := make(map[string]Node)
	for _, s := range nodesS {
		id, set, _ := strings.Cut(s, " = ")
		set = strings.Trim(set, "()")
		left, right, _ := strings.Cut(set, ", ")
		nodes[id] = Node{Left: left, Right: right}
	}

	p1Sum := 0
	p1CurrentNode := "AAA"
	for i := 0; ; i++ {
		instruction := instructions[i%len(instructions)]
		node := nodes[p1CurrentNode]
		if instruction == 0 {
			p1CurrentNode = node.Left
		} else {
			p1CurrentNode = node.Right
		}

		if p1CurrentNode == "ZZZ" {
			break
		}

		p1Sum++
	}

	var p2StartNodes []string
	for id, _ := range nodes {
		if strings.HasSuffix(id, "A") {
			p2StartNodes = append(p2StartNodes, id)
		}
	}

	p2Steps := make([]int, len(p2StartNodes))

	for startI, id := range p2StartNodes {
		for i := 0; ; i++ {
			instruction := instructions[i%len(instructions)]

			node := nodes[id]
			if instruction == 0 {
				id = node.Left
			} else {
				id = node.Right
			}
			p2Steps[startI]++

			if strings.HasSuffix(id, "Z") {
				break
			}
		}
	}

	gcd := func(a, b int) int {
		for b != 0 {
			a, b = b, a%b
		}
		return a
	}

	var lcm func(x ...int) int
	lcm = func(x ...int) int {
		if len(x) == 1 {
			return x[0]
		} else if len(x) > 2 {
			return lcm(x[0], lcm(x[1:]...))
		}
		return x[0] * x[1] / gcd(x[0], x[1])
	}

	p2Sum := lcm(p2Steps...)

	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
