package main

import (
	"log"
	"sort"
	"strconv"
	"strings"
)

func D07(input string) {
	lines := strings.Split(input, "\n")

	type Hand struct {
		Cards       map[rune]int
		CardsString string
		Bid         int
	}

	var hands []Hand
	for _, line := range lines {
		cardsS, bidS, _ := strings.Cut(line, " ")
		bid, _ := strconv.Atoi(bidS)
		hand := Hand{
			Cards:       make(map[rune]int),
			CardsString: cardsS,
			Bid:         bid,
		}

		for _, card := range cardsS {
			hand.Cards[card]++
		}
		hands = append(hands, hand)
	}

	p1RuneOrder := map[rune]int{
		'A': 13,
		'K': 12,
		'Q': 11,
		'J': 10,
		'T': 9,
		'9': 8,
		'8': 7,
		'7': 6,
		'6': 5,
		'5': 4,
		'4': 3,
		'3': 2,
		'2': 1,
	}

	p2RuneOrder := map[rune]int{
		'A': 13,
		'K': 12,
		'Q': 11,
		'T': 10,
		'9': 9,
		'8': 8,
		'7': 7,
		'6': 6,
		'5': 5,
		'4': 4,
		'3': 3,
		'2': 2,
		'J': 1,
	}

	highestType := func(m map[rune]int, p2 bool) int {
		h := 0
		pairs := 0
		jokers := 0
		strengths := 0

		for c, i := range m {
			if p2 && c == 'J' {
				jokers = i
				// There must be at least a pair. Doesn't influence other rankings, as pairs are evaluated last
				pairs++
				continue
			}
			if i > h {
				h = i
			}
			if i == 2 {
				pairs++
			}
			strengths++
		}

		h += jokers

		if h > 3 { // 4-5 of a kind
			return h
		}

		if strengths == 2 { // full house
			return 3
		}

		if h == 3 { // 3 of a kind
			return 2
		}

		return -1 + pairs // 1-2 pairs or high card
	}

	sortHands := func(hands []Hand, p2 bool) {
		sort.Slice(hands, func(a, b int) bool {
			typeA := highestType(hands[a].Cards, p2)
			typeB := highestType(hands[b].Cards, p2)

			if typeA != typeB {
				return typeA > typeB
			}

			for i, c := range hands[a].CardsString {
				if c != rune(hands[b].CardsString[i]) {
					order := p1RuneOrder
					if p2 {
						order = p2RuneOrder
					}
					return order[c] > order[rune(hands[b].CardsString[i])]
				}
			}

			return false
		})
	}

	p1Hands := make([]Hand, len(hands))
	p2Hands := make([]Hand, len(hands))
	copy(p1Hands, hands)
	copy(p2Hands, hands)
	sortHands(p1Hands, false)
	sortHands(p2Hands, true)

	p1Sum := 0
	for i, hand := range p1Hands {
		p1Sum += hand.Bid * (len(hands) - i)
	}
	p2Sum := 0
	for i, hand := range p2Hands {
		p2Sum += hand.Bid * (len(hands) - i)
	}

	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
