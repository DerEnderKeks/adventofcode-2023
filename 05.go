package main

import (
	"log"
	"math"
	"strconv"
	"strings"
)

func D05(input string) {
	lines := strings.Split(input, "\n")

	type MapEntry struct {
		SourceIndex int
		DestIndex   int
		Range       int
	}

	type Map []MapEntry

	type Range struct {
		Start int
		Range int
	}

	var p1Seeds []int
	var p2Seeds []Range
	var maps []Map

	inputSection := -1
	for _, line := range lines {
		if line == "" {
			continue
		}
		if strings.HasSuffix(line, ":") {
			inputSection++
			continue
		}
		if inputSection == -1 {
			_, seedsString, _ := strings.Cut(line, ": ")
			seedsStringSplit := strings.Split(seedsString, " ")
			currentRange := Range{}
			for i, s := range seedsStringSplit {
				atoi, _ := strconv.Atoi(s)
				p1Seeds = append(p1Seeds, atoi)
				if i%2 == 0 {
					currentRange.Start = atoi
				} else {
					currentRange.Range = atoi
					p2Seeds = append(p2Seeds, currentRange)
				}
			}
		} else {
			mapSplit := strings.Split(line, " ")
			var atoiEntry []int
			for _, s := range mapSplit {
				atoi, _ := strconv.Atoi(s)
				atoiEntry = append(atoiEntry, atoi)
			}
			mapEntry := MapEntry{
				SourceIndex: atoiEntry[1],
				DestIndex:   atoiEntry[0],
				Range:       atoiEntry[2],
			}
			if !(len(maps) > inputSection) {
				maps = append(maps, Map{})
			}
			maps[inputSection] = append(maps[inputSection], mapEntry)
		}
	}

	findSmallestLocation := func(seed int, maps []Map, result *int) {
		nextSrc := seed
		for _, m := range maps {
			for _, entry := range m {
				if entry.SourceIndex <= nextSrc && nextSrc <= (entry.SourceIndex+entry.Range-1) {
					nextSrc = entry.DestIndex + (nextSrc - entry.SourceIndex)
					break
				}
			}
		}
		if nextSrc < *result {
			*result = nextSrc
		}
	}

	p1Sum := math.MaxInt
	p2Sum := math.MaxInt

	for _, seed := range p1Seeds {
		findSmallestLocation(seed, maps, &p1Sum)
	}
	for _, seed := range p2Seeds {
		for i := seed.Start; i < (seed.Start + seed.Range - 1); i++ {
			findSmallestLocation(i, maps, &p2Sum)
		}
	}

	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
