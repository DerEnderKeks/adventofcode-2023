package main

import (
	"log"
	"strconv"
	"strings"
)

func D02(input string) {
	var p1PossibleGames []int
	var p2SubSums []int
	p1Sum := 0
	p2Sum := 0

	type CubeSet struct {
		Red   int
		Green int
		Blue  int
	}

	type Game struct {
		ID      int
		Reveals []CubeSet
		Peaks   CubeSet
	}

	var games []Game

	limit := CubeSet{
		Red:   12,
		Green: 13,
		Blue:  14,
	}

	for _, s := range strings.Split(input, "\n") {
		game := Game{
			Peaks: CubeSet{},
		}

		gameInfo, reveals, _ := strings.Cut(s, ": ")
		_, idString, _ := strings.Cut(gameInfo, " ")
		game.ID, _ = strconv.Atoi(idString)

		for _, revealString := range strings.Split(reveals, "; ") {
			cubeSet := CubeSet{}
			for _, colorString := range strings.Split(revealString, ", ") {
				countS, color, _ := strings.Cut(colorString, " ")
				count, _ := strconv.Atoi(countS)
				switch color {
				case "red":
					cubeSet.Red = count
					if count > game.Peaks.Red {
						game.Peaks.Red = count
					}
				case "green":
					cubeSet.Green = count
					if count > game.Peaks.Green {
						game.Peaks.Green = count
					}
				case "blue":
					cubeSet.Blue = count
					if count > game.Peaks.Blue {
						game.Peaks.Blue = count
					}
				}
			}
			game.Reveals = append(game.Reveals, cubeSet)
		}
		games = append(games, game)
		if game.Peaks.Red <= limit.Red && game.Peaks.Green <= limit.Green && game.Peaks.Blue <= limit.Blue {
			p1PossibleGames = append(p1PossibleGames, game.ID)
			p1Sum += game.ID
		}
		p2GameSum := game.Peaks.Red * game.Peaks.Green * game.Peaks.Blue
		p2Sum += p2GameSum
		p2SubSums = append(p2SubSums, p2GameSum)
	}
	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
