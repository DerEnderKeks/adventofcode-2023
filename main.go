package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"strconv"
)

type DaySolver func(string)

func main() {
	if len(os.Args) < 2 {
		log.Fatalln("Day missing")
	}
	day, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatalln("Failed to parse day:", err)
	}

	dayMap := map[int]DaySolver{
		1: D01,
		2: D02,
		3: D03,
		4: D04,
		5: D05,
		6: D06,
		7: D07,
		8: D08,
	}

	if dayFunc, ok := dayMap[day]; ok {
		file, err := os.ReadFile(path.Join("inputs", fmt.Sprintf("%02d.txt", day)))
		if err != nil {
			log.Fatalln(fmt.Sprintf("Failed to read input for day %d:", day), err)
			return
		}
		dayFunc(string(file))
	} else {
		log.Fatalln(fmt.Sprintf("Day %d not implemented", day))
	}
}
