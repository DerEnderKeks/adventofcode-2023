package main

import (
	"log"
	"math"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

func D04(input string) {
	lines := strings.Split(input, "\n")
	type Card struct {
		ID      int
		Matches int
		Count   int
	}

	splitRegex := regexp.MustCompile("\\s+")

	var cards []Card
	for _, line := range lines {
		cardInfo, content, _ := strings.Cut(line, ":")
		idS := splitRegex.Split(cardInfo, 2)
		winnersS, numbersS, _ := strings.Cut(content, "|")

		winnersS = strings.Trim(winnersS, " ")
		numbersS = strings.Trim(numbersS, " ")

		var winners []int
		for _, s := range splitRegex.Split(winnersS, -1) {
			atoi, _ := strconv.Atoi(s)
			winners = append(winners, atoi)
		}
		var matches []int
		for _, s := range splitRegex.Split(numbersS, -1) {
			atoi, _ := strconv.Atoi(s)
			if !slices.Contains(winners, atoi) {
				continue
			}
			matches = append(matches, atoi)
		}
		id, _ := strconv.Atoi(idS[1])
		card := Card{
			ID:      id,
			Matches: len(matches),
			Count:   1,
		}
		cards = append(cards, card)
	}

	p1Sum := 0
	for _, card := range cards {
		if card.Matches > 1 {
			p1Sum += int(math.Pow(2, float64(card.Matches-1)))
		} else {
			p1Sum += card.Matches
		}
	}

	p2Sum := 0
	for i, card := range cards {
		for c := 0; c < card.Count; c++ {
			for j := 1; j < card.Matches+1; j++ {
				if i+j > len(cards) {
					break
				}
				cards[i+j].Count += 1
			}
		}
		p2Sum += card.Count
	}

	log.Printf("P1: %d", p1Sum)
	log.Printf("P2: %d", p2Sum)
}
